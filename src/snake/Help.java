package snake;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Help extends JPanel {

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// Draws help screen
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 600, 500);

		// Heading
		Font font = new Font("Monospaced", Font.BOLD, 50);
		g.setColor(new Color(102, 26, 255));
		g.setFont(font);
		g.drawString("Instructions:", 20, 70);

		// How to move etc
		Font font2 = new Font("Monospaced", Font.PLAIN, 20);
		g.setColor(Color.BLACK);
		g.setFont(font2);
		g.drawString("\u21E7", 20, 120);
		g.drawString("- Moving up", 50, 120);
		g.drawString("\u21E9", 20, 120 + 30);
		g.drawString("- Moving down", 50, 120 + 30);
		g.drawString("\u21E8", 20, 120 + 30 * 2);
		g.drawString("- Moving right", 50, 120 + 30 * 2);
		g.drawString("\u21E6", 20, 120 + 30 * 3);
		g.drawString("- Moving left", 50, 120 + 30 * 3);

		// What to eat and what not to eat
		g.setColor(Color.RED);
		g.fillRect(25, 120 + 30 * 5 - 10, 10, 10);
		g.setColor(Color.BLACK);
		g.drawString("- Treats", 50, 120 + 30 * 5);

		g.setColor(Color.YELLOW);
		g.fillRect(20, 120 + 30 * 6 - 13, 20, 20);
		g.setColor(Color.BLACK);
		g.drawString("- Big treats", 50, 120 + 30 * 6);

		g.fillRect(20, 120 + 30 * 7 - 12, 20, 20);
		g.drawString("- Bombs", 50, 120 + 30 * 7);

		// Levels
		g.drawString("1. Level is with random bombs", 20, 120 + 30 * 9);
		g.drawString("2. Level is with bomb walls", 20, 120 + 30 * 10);
		g.drawString("3. Level is with more bomb walls", 20, 120 + 30 * 11);
	}

}
