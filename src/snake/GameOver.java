package snake;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Collections;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GameOver extends JPanel {

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// Draws background when game over
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 600, 500);

		// Gives text "Game over" and shows High Score
		Font font = new Font("Monospaced", Font.BOLD, 74);
		MainSnake text = MainSnake.mainSnake;
		g.setColor(Color.WHITE);
		g.setFont(font);
		g.drawString("GAME OVER ", 30, 100);

		Font font2 = new Font("Monospaced", Font.PLAIN, 30);
		g.setFont(font2);
		g.drawString("Press SPACE to start again!", 20, 185);

		// This shows 5 high scores
		g.drawString("HIGH SCORES:", 20, 245);

		// Sort high score array list and give it bigger to smaller
		Collections.sort(text.highScores, new HighscoreScoreComparator().reversed());

		for (int i = 1; i < 6; i++) {
			String name = "NONE";
			int score = 0;

			// When there are not enough scores (5), it will still work
			try {
				HighScore curHighScore = text.highScores.get(i - 1);
				name = curHighScore.getName();
				score = curHighScore.getScore();
			} catch (Exception e) {
				// Invalid index of text.highScores
			}

			if (name.equals("NONE")) {
				// In case there is no score, fill the row with "i. "
				g.drawString(i + ". ", 20, 245 + 40 * i);
			} else {
				// If there are scores to show, this will print them on screen
				g.drawString(i + ". " + name + " " + score + "p", 20, 245 + 40 * i);
			}
		}
	}
}
