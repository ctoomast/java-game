package snake;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Drawings extends JPanel {

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		MainSnake snake = MainSnake.mainSnake;

		// Fills Top
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 600, 30);

		// Shows Score
		Font font = new Font("Monospaced", Font.BOLD, 20);
		g.setFont(font);
		g.setColor(Color.BLACK);
		String scoreStr = "Score: " + snake.score;
		g.drawString(scoreStr, 420, 25);

		// Fills background
		g.setColor(new Color(52292));
		g.fillRect(0, 30, 600, 470);

		// Draws Snake
		g.setColor(new Color(16757721));
		for (Point point : snake.snakeBody) {
			g.fillRect(point.x * MainSnake.SCALE, point.y * MainSnake.SCALE, MainSnake.SCALE, MainSnake.SCALE);
		}
		g.fillRect(snake.snakeHead.x * MainSnake.SCALE, snake.snakeHead.y * MainSnake.SCALE, MainSnake.SCALE,
				MainSnake.SCALE);

		// Draws Treat
		g.setColor(Color.RED);
		g.fillRect(snake.treat.x * MainSnake.SCALE, snake.treat.y * MainSnake.SCALE, MainSnake.SCALE, MainSnake.SCALE);

		// Draws Big Treat
		g.setColor(Color.YELLOW);
		g.fillRect(snake.bigTreat.x * MainSnake.SCALE, snake.bigTreat.y * MainSnake.SCALE, MainSnake.SCALE * 2,
				MainSnake.SCALE * 2);

		// Draws bomb for level 1
		g.setColor(Color.BLACK);
		g.fillRect(snake.bomb.x * MainSnake.SCALE, snake.bomb.y * MainSnake.SCALE, MainSnake.SCALE * 2,
				MainSnake.SCALE * 2);

		// Draws bomb walls for level 2
		g.fillRect(snake.bombWall.x * MainSnake.SCALE, snake.bombWall.y * MainSnake.SCALE, MainSnake.SCALE * 2,
				MainSnake.SCALE * 30);
		g.fillRect((snake.bombWall.x + 38) * MainSnake.SCALE, snake.bombWall.y * MainSnake.SCALE, MainSnake.SCALE * 2,
				MainSnake.SCALE * 30);

		// Draws bomb walls for level 3
		g.fillRect(snake.bombWalls3.x * MainSnake.SCALE, snake.bombWalls3.y * MainSnake.SCALE, MainSnake.SCALE * 2,
				MainSnake.SCALE * 22);
		g.fillRect((snake.bombWalls3.x + 42) * MainSnake.SCALE, snake.bombWalls3.y * MainSnake.SCALE,
				MainSnake.SCALE * 2, MainSnake.SCALE * 22);
		g.fillRect((snake.bombWalls3.x + 10) * MainSnake.SCALE, (snake.bombWalls3.y - 6) * MainSnake.SCALE,
				MainSnake.SCALE * 24, MainSnake.SCALE * 2);
		g.fillRect((snake.bombWalls3.x + 10) * MainSnake.SCALE, (snake.bombWalls3.y + 26) * MainSnake.SCALE,
				MainSnake.SCALE * 24, MainSnake.SCALE * 2);
		g.fillRect((snake.bombWalls3.x + 20) * MainSnake.SCALE, (snake.bombWalls3.y + 8) * MainSnake.SCALE,
				MainSnake.SCALE * 4, MainSnake.SCALE * 6);

	}

}
