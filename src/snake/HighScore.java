package snake;

import java.util.Comparator;

public class HighScore {

	public String name;
	public int score;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "HighScore [name=" + name + ", score=" + score + "]";
	}
}

// Compares scores and returns positive if h1 is higher, negative if h2 is
// higher
class HighscoreScoreComparator implements Comparator<HighScore> {
	public int compare(HighScore h1, HighScore h2) {
		return h1.getScore() - h2.getScore();
	}
}