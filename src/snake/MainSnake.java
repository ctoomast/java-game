package snake;

/**
 * Some basics in doing snake game has been taken from this site https://www.youtube.com/watch?v=S_n3lryyGZM
 */

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.Timer;

public class MainSnake implements ActionListener, KeyListener {

    // Frame variables
    public JFrame gameFrame;
    public static MainSnake mainSnake;
    public Dimension dim;

    // Different screens
    public Drawings drawings;
    public GameOver gameOverScreen;
    public Help helpScreen;
    public StartScreen startScreen;

    // Game scoring variables
    public ArrayList<HighScore> highScores = new ArrayList<HighScore>();
    public ArrayList<String> playerName = new ArrayList<String>();
    public String curName;

    // Game object variables
    public ArrayList<Point> snakeBody = new ArrayList<Point>();
    public Point snakeHead, treat, bomb, bigTreat, bombWall, bombWalls3;
    public static final int UP = 8, DOWN = 2, LEFT = 4, RIGHT = 6, SCALE = 10;
    public int direction, score, snakeLength, level, treatNum, num, speedDivided;

    public long lastBombSpawned;
    public Random rand;

    // Game state
    public boolean gameOver, helpNeeded, gameStarted = false, treatLocation;
    public Timer timer = new Timer(20, this);

    // Every time game starts it will implement these things to frame
    @SuppressWarnings("static-access")
    public MainSnake() {
        dim = Toolkit.getDefaultToolkit().getScreenSize();
        gameFrame = new JFrame("Snake");
        gameFrame.setVisible(true);
        gameFrame.setSize(600, 500);
        gameFrame.setResizable(false);
        gameFrame.setLocation(dim.width / 2 - gameFrame.getWidth() / 2, dim.height / 2 - gameFrame.getHeight() / 2);
        gameFrame.add(startScreen = new StartScreen());
        gameFrame.setDefaultCloseOperation(gameFrame.EXIT_ON_CLOSE);
        gameFrame.addKeyListener(this);
    }

    // Everything goes back, when new game is started.
    public void startGame() {

        // Game frame variables
        gameFrame.add(drawings = new Drawings());
        gameFrame.setVisible(true);

        // Variables that go back into said state, when game starts
        gameStarted = true;
        gameOver = false;
        helpNeeded = false;
        score = 0;
        num = 0;

        // Snake variables
        snakeLength = 1;
        speedDivided = 5;
        snakeHead = new Point(0, 3);
        snakeBody.clear();
        direction = DOWN;

        // Treats variables
        rand = new Random();
        treat = new Point(-2, -2);
        treatNum = 1;
        bigTreat = new Point(-2, -2);

        // Bomb and bomb wall variables
        bomb = new Point(-2, -2);
        bombWall = new Point(-60, -50);
        bombWalls3 = new Point(-60, -50);
        lastBombSpawned = System.currentTimeMillis();

        //
        for (int i = 0; i < snakeLength; i++) {
            snakeBody.add(new Point(snakeHead.x, snakeHead.y));
        }

        // Timer starts
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (level == 1) {
            if (!gameOver) {

                repaint();

                // Spawn a bomb if the last one was spawned over 20 seconds ago
                if (System.currentTimeMillis() - lastBombSpawned > 10000) {
                    bomb = new Point(-2, -2);
                }

                // Hide the bomb if it has been showing for 10 seconds
                if (System.currentTimeMillis() - lastBombSpawned > 20000) {
                    bomb = new Point(rand.nextInt(56), rand.nextInt(41) + 3);
                    lastBombSpawned = System.currentTimeMillis();
                }

                // Gives speed for snake and controls if there is a snake head
                if (num % speedDivided == 0 && snakeHead != null) {

                    snakeControl();

                    // Goes into, when there is a treat
                    if (treat != null) {

                        // Draws first treat
                        if (treatNum == 1) {
                            treat.setLocation(rand.nextInt(58), rand.nextInt(43) + 3);
                            treatNum++;
                        }

                        // Checks if snake catches a treat
                        if (snakeHead.equals(treat) || bigPointBool(bigTreat)) {

                            scoreLogic();

                            removeTreats();

                            // Create the next treat on the field
                            if ((snakeLength + 1) % 6 == 0) {
                                // Every fifth treat is a big one
                                bigTreat.setLocation(rand.nextInt(56), rand.nextInt(41) + 3);
                                treatNum++;
                            } else {
                                // Other treats are normal ones
                                treat.setLocation(rand.nextInt(58), rand.nextInt(43) + 3);
                                treatNum++;
                            }
                        }
                    }

                    // If snake head touches bomb, then game is over
                    if (bigPointBool(bomb)) {
                        gameOver = true;
                    }

                }
            } else {
                gameOver();
            }
        }

        if (level == 2) {
            if (!gameOver) {

                repaint();

                // Draws bomb wall with its starting point
                bombWall = new Point(10, 10);

                // Gives speed for snake and controls if there is a snake head
                if (num % speedDivided == 0 && snakeHead != null) {

                    snakeControl();

                    // Goes into, when there is a treat
                    if (treat != null) {
                        // Draws first treat and checks where it is
                        if (treatNum == 1) {
                            lvl2TreatSpawnError();
                        }

                        // Checks if snake catches a treat
                        if (snakeHead.equals(treat) || bigPointBool(bigTreat)) {

                            scoreLogic();

                            removeTreats();

                            // create the next treat on the field
                            if ((snakeLength + 1) % 6 == 0) {
                                // Treat can't spawn into bomb wall
                                // Every fifth treat is a big one
                                treatLocation = false;
                                do {
                                    bigTreat.setLocation(rand.nextInt(56), rand.nextInt(41) + 3);
                                    if (bigTreat.x > 8 && bigTreat.x < 14 && bigTreat.y > 7 && bigTreat.y < 42) {
                                        treatLocation = false;
                                    } else if (bigTreat.x > 46 && bigTreat.x < 52 && bigTreat.y > 7 && bigTreat.y < 42) {
                                        treatLocation = false;
                                    } else {
                                        // If treat isn't on any wall, it will
                                        // be drawn
                                        // otherwise it will be given a new
                                        // location
                                        treatLocation = true;
                                        treatNum++;
                                        break;
                                    }
                                } while (treatLocation = true);

                            } else {
                                lvl2TreatSpawnError();
                            }
                        }

                    }

                    // Collision with walls
                    if (snakeHead.x + 1 > 10 && snakeHead.x < 12 && snakeHead.y > 9 && snakeHead.y < 40) {
                        gameOver = true;
                    }
                    if (snakeHead.x + 1 > 48 && snakeHead.x < 50 && snakeHead.y > 9 && snakeHead.y < 40) {
                        gameOver = true;
                    }
                }

            } else {
                gameOver();
            }
        }

        if (level == 3) {
            if (!gameOver) {

                repaint();

                // Draws bomb wall with its starting point
                bombWalls3.setLocation(8, 14);

                // Gives speed for snake and controls if there is a snake head
                if (num % speedDivided == 0 && snakeHead != null) {

                    snakeControl();

                    // When there is a treat on screen
                    if (treat != null) {
                        // Draws a first treat and checks that it's not on bomb
                        // walls
                        if (treatNum == 1) {
                            lvl3TreatSpawnError();
                        }

                        // Checks if snake catches a treat
                        if (snakeHead.equals(treat) || bigPointBool(bigTreat)) {

                            scoreLogic();

                            removeTreats();

                            // Create the next treat on the field
                            if ((snakeLength + 1) % 6 == 0) {
                                // Treat can't spawn on bomb walls
                                // Every fifth treat is a big one
                                treatLocation = false;
                                do {
                                    bigTreat.setLocation(rand.nextInt(56), rand.nextInt(41) + 3);
                                    if (bigTreat.x > 5 && bigTreat.x < 13 && bigTreat.y > 11 && bigTreat.y < 38) {
                                        treatLocation = false;
                                    } else if (bigTreat.x > 47 && bigTreat.x < 55 && bigTreat.y > 11 && bigTreat.y < 38) {
                                        treatLocation = false;
                                    } else if (bigTreat.x > 15 && bigTreat.x < 45 && bigTreat.y > 6
                                            && bigTreat.y < 12) {
                                        treatLocation = false;
                                    } else if (bigTreat.x > 15 && bigTreat.x < 45 && bigTreat.y > 37 && bigTreat.y < 44) {
                                        treatLocation = false;
                                    } else if (bigTreat.x > 25 && bigTreat.x < 34 && bigTreat.y > 20
                                            && bigTreat.y > 30) {
                                        treatLocation = false;
                                    } else {
                                        // If treat isn't on any wall, it will
                                        // be drawn
                                        // otherwise it will be given a new
                                        // location
                                        treatLocation = true;
                                        treatNum++;
                                        break;
                                    }
                                } while (treatLocation = false);

                            } else {
                                lvl3TreatSpawnError();
                            }
                        }
                    }

                    // Collision with walls
                    if (snakeHead.x + 1 > 8 && snakeHead.x < 10 && snakeHead.y > 13 && snakeHead.y < 36) {
                        gameOver = true;
                    }
                    if (snakeHead.x + 1 > 50 && snakeHead.x < 52 && snakeHead.y > 13 && snakeHead.y < 36) {
                        gameOver = true;
                    }
                    if (snakeHead.x + 1 > 18 && snakeHead.x < 42 && snakeHead.y > 7 && snakeHead.y < 10) {
                        gameOver = true;
                    }
                    if (snakeHead.x + 1 > 18 && snakeHead.x < 42 && snakeHead.y > 39 && snakeHead.y < 42) {
                        gameOver = true;
                    }
                    if (snakeHead.x + 1 > 28 && snakeHead.x < 32 && snakeHead.y > 21 && snakeHead.y < 28) {
                        gameOver = true;
                    }
                }

            } else {
                gameOver();
            }
        }

    }

    // This method helps to check bigger objects collision with snakeHead (2*2
    // objects)
    private boolean bigPointBool(Point point) {

        Point currentPoint = new Point(point.x, point.y);
        if (snakeHead.equals(currentPoint)) {
            return true;
        }
        currentPoint.x += 1;
        if (snakeHead.equals(currentPoint)) {
            return true;
        }
        currentPoint.y += 1;
        if (snakeHead.equals(currentPoint)) {
            return true;
        }
        currentPoint.x -= 1;
        if (snakeHead.equals(currentPoint)) {
            return true;
        }
        return false;
    }

    // Returns true if no snake body point is at i, y coordinates
    private boolean noTailAt(int i, int y) {
        for (Point point : snakeBody) {
            if (point.equals(new Point(i, y))) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        mainSnake = new MainSnake();
    }

    // On keyboard pressing keys u get different thing done
    // and these are told here
    @Override
    public void keyPressed(KeyEvent e) {
        int i = e.getKeyCode();

        // Pressing left arrow makes snake to go left
        // Does nothing when snake is moving to right
        if (i == KeyEvent.VK_LEFT && direction != RIGHT) {
            direction = LEFT;
            return;
        }

        // Pressing right arrow makes snake to go right
        // Does nothing when snake is moving to left
        if (i == KeyEvent.VK_RIGHT && direction != LEFT) {
            direction = RIGHT;
            return;
        }

        // Pressing up arrow makes snake to go up
        // Does nothing when snake is moving to down
        if (i == KeyEvent.VK_UP && direction != DOWN) {
            direction = UP;
            return;
        }

        // Pressing down arrow makes snake to go down
        // Does nothing when snake is moving to up
        if (i == KeyEvent.VK_DOWN && direction != UP) {
            direction = DOWN;
            return;
        }

        // Space starts game
        if (i == KeyEvent.VK_SPACE) {
            // Game starts only, when there is name at least 3 letters long and
            // level is chosen
            if (playerName.size() > 2 && level > 0) {
                if (gameOver) {
                    // If game is over, then space will take player to start
                    // screen
                    gameOver = false;
                    gameStarted = false;
                    gameFrame.add(startScreen = new StartScreen());
                    gameFrame.setVisible(true);

                } else if (!gameStarted) {
                    // If player in on start screen and presses space, game will
                    // start
                    curName = String.join("", playerName);
                    startGame();
                }
            }
            return;
        }

        // Player can insert name
        if (Character.isAlphabetic(i)) {
            if (!gameStarted) {
                // Checks if player name is not too long and makes sure player
                // can't use numpad
                if (playerName.size() < 20 && !KeyEvent.getKeyText(i).contains("NumPad")) {
                    startScreen.repaint();
                    playerName.add(KeyEvent.getKeyText(i));
                }
            }
            return;
        }
        if (i == KeyEvent.VK_BACK_SPACE) {
            // Player can delete name and write a new one
            if (!gameStarted) {
                if (playerName.size() > 0) {
                    startScreen.repaint();
                    playerName.remove(playerName.size() - 1);
                    return;
                }
            }
        }

        // Shift key is used to see help screen
        if (i == KeyEvent.VK_SHIFT) {
            // If game is not started and help screen is not open, pressing
            // shift will open it
            if (!helpNeeded && !gameStarted) {
                gameFrame.add(helpScreen = new Help());
                gameFrame.setVisible(true);
                helpNeeded = true;
                return;

            } else if (helpNeeded && !gameStarted) {
                // if help screen is open, this will close it and goes back to
                // start screen
                helpNeeded = !helpNeeded;
                gameFrame.add(startScreen = new StartScreen());
                gameFrame.setVisible(true);
                return;
            }
        }

        // Key Event for choosing levels
        if (i == KeyEvent.VK_1) {
            // Level 1
            if (!gameStarted) {
                level = 1;
                startScreen.repaint();
            }
            return;
        }

        if (i == KeyEvent.VK_2) {
            // Level 2
            if (!gameStarted) {
                level = 2;
                startScreen.repaint();
            }
            return;
        }

        if (i == KeyEvent.VK_3) {
            // Level 3
            if (!gameStarted) {
                level = 3;
                startScreen.repaint();
            }
            return;
        }
    }

    public void repaint() {
        // Always repaint screen
        drawings.repaint();
        num++;
    }

    public void gameOver() {
        // Game over
        // Saves game score with player name and puts it in highScore
        // array
        HighScore curHighScore = new HighScore();
        curHighScore.setName(curName);
        curHighScore.setScore(score);
        highScores.add(curHighScore);

        // Goes to game over screen
        gameFrame.add(gameOverScreen = new GameOver());
        gameFrame.setVisible(true);

        // Timer stops
        timer.stop();
    }

    public void removeTreats() {
        // remove all treats on the field
        treat.setLocation(-1, -1);
        bigTreat.setLocation(-2, -2);
    }

    public void snakeControl() {
        // Adds new point to snake
        snakeBody.add(new Point(snakeHead.x, snakeHead.y));

        // Controls if snake is inside frames and not touching its
        // body
        if (direction == UP) {
            if (snakeHead.y - 1 >= 3 && noTailAt(snakeHead.x, snakeHead.y - 1)) {
                snakeHead = new Point(snakeHead.x, snakeHead.y - 1);
            } else {
                gameOver = true;
            }
        }

        if (direction == DOWN) {
            if (snakeHead.y + 1 < 47 && noTailAt(snakeHead.x, snakeHead.y + 1)) {
                snakeHead = new Point(snakeHead.x, snakeHead.y + 1);
            } else {
                gameOver = true;
            }
        }

        if (direction == LEFT) {
            if (snakeHead.x - 1 >= 0 && noTailAt(snakeHead.x - 1, snakeHead.y)) {
                snakeHead = new Point(snakeHead.x - 1, snakeHead.y);
            } else {
                gameOver = true;
            }
        }

        if (direction == RIGHT) {
            if (snakeHead.x + 1 < 59 && noTailAt(snakeHead.x + 1, snakeHead.y)) {
                snakeHead = new Point(snakeHead.x + 1, snakeHead.y);
            } else {
                gameOver = true;
            }
        }

        // Removes a point, when snakeBody array has more elements
        // than snakeLength variable
        if (snakeBody.size() > snakeLength) {
            snakeBody.remove(0);
        }
    }

    public void lvl2TreatSpawnError() {
        // Treat can't spawn into bomb wall
        treatLocation = false;
        do {
            treat.setLocation(rand.nextInt(58), rand.nextInt(43) + 3);
            if (treat.x > 9 && treat.x < 13 && treat.y > 8 && treat.y < 41) {
                treatLocation = false;
            } else if (treat.x > 47 && treat.x < 51 && treat.y > 8 && treat.y < 41) {
                treatLocation = false;
            } else {
                // If treat isn't on any wall, it will
                // be drawn
                // otherwise it will be given a new
                // location
                treatLocation = true;
                treatNum++;
                break;
            }

        } while (treatLocation = true);
    }

    public void lvl3TreatSpawnError() {
        treatLocation = false;
        do {
            treat.setLocation(rand.nextInt(58), rand.nextInt(43) + 3);
            if (treat.x > 7 && treat.x < 11 && treat.y > 13 && treat.y < 37) {
                treatLocation = false;
            } else if (treat.x > 49 && treat.x < 53 && treat.y > 13 && treat.y < 37) {
                treatLocation = false;
            } else if (treat.x > 17 && treat.x < 43 && treat.y > 7 && treat.y < 11) {
                treatLocation = false;
            } else if (treat.x > 17 && treat.x < 43 && treat.y > 39 && treat.y < 43) {
                treatLocation = false;
            } else if (treat.x > 27 && treat.x < 33 && treat.y > 21 && treat.y < 29) {
                treatLocation = false;
            } else {
                // If treat isn't on any wall, it will be
                // drawn
                // otherwise it will be given a new location
                treatLocation = true;
                treatNum++;
                break;
            }
        } while (treatLocation = true);
    }

    public void scoreLogic() {
        // Score logic
        if (bigPointBool(bigTreat)) {
            // If snake eats bigTreat
            score += 20;
            snakeLength += 2;

            // Changes snake speed twice
            if (score == 60) {
                // When snake eats first bigTreat
                speedDivided -= 1;
            } else if (score == 120) {
                // When snake eats second bigTreat
                speedDivided -= 1;
            }

        } else {
            // If snake eats normal treat
            score += 10;
            snakeLength++;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

}
