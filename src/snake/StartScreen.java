package snake;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class StartScreen extends JPanel {

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// Draws first screen
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 600, 500);

		MainSnake text = MainSnake.mainSnake;
		// Writes text
		g.setColor(Color.BLACK);
		Font font = new Font("Monospaced", Font.BOLD, 74);
		g.setFont(font);
		g.drawString("SNAKE", 40, 30 + 74);

		// Writes other important information
		Font font2 = new Font("Monospaced", Font.PLAIN, 22);
		g.setFont(font2);

		g.drawString("For help press SHIFT!", 20, text.gameFrame.getHeight() / 2 - 35);

		// Choose level
		g.drawString("Choose level by pressing 1 2 or 3", 20, text.gameFrame.getHeight() / 2 + 35);
		g.drawString("You chose level " + text.level, 40, text.gameFrame.getHeight() / 2 + 35 * 2);

		// Prints input on screen.
		g.drawString("Enter your name: " + String.join("", text.playerName), 20,
				text.gameFrame.getHeight() / 2 + 35 * 3);

		// Press space to start
		g.drawString("Press SPACE to start!", 20, text.gameFrame.getHeight() / 2 + 35 * 5);
	}

}